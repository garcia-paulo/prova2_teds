package prova2.prova2;

public class FizzBuzz {
	 
    private static final int CINCO = 5;
    private static final int TRES = 3;
 
    public String calcular(int numero) {
 
        if (ehDivisivel(numero, TRES) && ehDivisivel(numero, CINCO)) {
            return "FizzBuzz";
        }
        if (ehDivisivel(numero, TRES)) {
            return "Fizz";
        }
        if (ehDivisivel(numero, CINCO)) {
            return "Buzz";
        }
        return "" + numero;
    }
 
    private boolean ehDivisivel(int dividendo, int divisor) {
        return dividendo % divisor == 0;
    }
}
