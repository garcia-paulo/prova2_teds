package prova2.prova2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PraQueMesmoTest {

	@Test
	void testMaiorQueTrinta() {
		PraQueMesmo praQue = new PraQueMesmo();
		assertEquals(40*4, praQue.naoFacoIdeia(40));
	}
	

	@Test
	void testMaiorQueDez() {
		PraQueMesmo praQue = new PraQueMesmo();
		assertEquals(30*3, praQue.naoFacoIdeia(30));
	}
	
	@Test
	void testIgualADez() {
		PraQueMesmo praQue = new PraQueMesmo();
		assertEquals(10*2, praQue.naoFacoIdeia(10));
	}
	
	@Test
	void testOutro() {
		PraQueMesmo praQue = new PraQueMesmo();
		assertEquals(5*2, praQue.naoFacoIdeia(5));
	}
}
